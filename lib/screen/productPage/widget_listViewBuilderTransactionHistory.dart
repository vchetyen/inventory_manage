import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/screen/productPage/widget_alertDialog.dart';
import 'package:inventory_manage/screen/productPage/dataProductPage.dart';

class ListViewTransactionHistory extends StatelessWidget {
  final int selectedIndex, index;
  ListViewTransactionHistory({this.selectedIndex, this.index});
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SliverList(
      // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      //   crossAxisCount: 1,
      //   mainAxisSpacing: 0.0,
      //   crossAxisSpacing: 0.0,
      //   childAspectRatio: 1.7,
      // ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int indexs) {
          return Container(
            //color: Colors.orange,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Date (Today, Yesterday ect.)
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                  child: Text(
                      allProducts[selectedIndex].detail[index]
                          ['transactionHistory'][indexs]['date'],
                      style: GoogleFonts.poppins(
                        fontSize: 14,
                      )),
                ),

                //Generate horizontal list of Container that contains Row(time,stockInOrOut,MinusOrAdd)
                Column(
                  //Length
                  children: List.generate(
                    allProducts[selectedIndex]
                        .detail[index]['transactionHistory'][indexs]['detail']
                        .length,
                    (indexx) => InkWell(
                      onTap: () {
                        AlertDialogWidget(
                                index: index,
                                indexs: indexs,
                                indexx: indexx,
                                selectedIndex: selectedIndex)
                            .alertDialogWidget(context);
                        //alertDialogWidget(context);
                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (BuildContext context) {
                        //   return alertDialogWidget();
                        // }));
                        print(allProducts[selectedIndex].detail[index]
                            ['transactionHistory'][indexs]['date']);
                        print(
                          allProducts[selectedIndex].detail[index]
                                  ['transactionHistory'][indexs]['detail']
                              [indexx]['time'],
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        margin: const EdgeInsets.only(top: 1),
                        color: Colors.white,
                        height: size.height * 0.07,
                        //Row that contains (Time,StockInOrOut,MinusOrAdd)
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //Time
                            Text(
                                allProducts[selectedIndex].detail[index]
                                        ['transactionHistory'][indexs]['detail']
                                    [indexx]['time'],
                                style: GoogleFonts.poppins(
                                    fontSize: 11, color: Colors.grey)),
                            //Stok In or Out
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['stockInOrOut'],
                              style: GoogleFonts.poppins(
                                  fontSize: 15, color: Colors.grey),
                            ),
                            SizedBox(width: 20),

                            //amount of Minus or add from Stock
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['amountOfAddOrMinus'],
                              style: GoogleFonts.poppins(
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        //Select total of TransactionHistopry length of every product depends on we click
        childCount: allProducts[selectedIndex]
            .detail[index]['transactionHistory']
            .length,
      ),
    );
  }
}
