import 'package:flutter/material.dart';

class Delegate extends SliverPersistentHeaderDelegate {
  final Widget child;
  final double maxHeight, minHeight;
  Delegate({this.child, this.maxHeight, this.minHeight});
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => maxHeight;

  @override
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true; //if false, it won't refresh
  }
}
