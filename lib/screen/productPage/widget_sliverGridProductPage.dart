import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/productPage/dataProductPage.dart';
import 'package:inventory_manage/screen/productPage/productDetailPage.dart';

import 'dataProductPage.dart';

//Template SliverGridView for product
class SliverGridProductPage extends StatelessWidget {
  final int selectedIndex;
  SliverGridProductPage({this.selectedIndex});
  @override
  Widget build(BuildContext context) {
    //Padding of the whole SliverGrid
    return SliverPadding(
      padding: const EdgeInsets.all(20),
      sliver: SliverGrid(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 0.84,
            crossAxisCount: 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),

        //Here it is. This is like GridView.builder
        delegate: SliverChildBuilderDelegate((_, index) {
          //Return any child widget so that it will generate it base on the amount of childCount

          return InkWell(
            onTap: () {
              print('go to product detail page');
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                print(
                    'SelectedIndex: ${selectedIndex} Index: $index name: ${allProducts[selectedIndex].detail[index]['name'].toString().toUpperCase()} category: ${allProducts[selectedIndex].detail[index]['category']}');

                return ProductDetailPage(
                  index: index,
                  selectedIndex: selectedIndex,
                );
              }));
            },
            //This Container wraps every box in GridView
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),

                //Column in each box in GridView, it divides Picture of product and Detail Products under it.
                child: Column(
                  children: [
                    //Picture of every product
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(allProducts[selectedIndex]
                                    .detail[index]['pic']))),
                      ),
                    ),

                    //Detail of every product
                    Padding(
                      padding: const EdgeInsets.all(8.0),

                      //Column inside Details product under the Picture
                      //It has Name product,Code product and the last Row(Category+Weight)
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //Name product
                          Text(
                            allProducts[selectedIndex]
                                .detail[index]['name']
                                .toString()
                                .toUpperCase(),
                            style: GoogleFonts.poppins(
                                letterSpacing: 2,
                                color: AppTheme.nearlyBlack,
                                fontSize: 16),
                          ),

                          //Code product
                          Text(
                            allProducts[selectedIndex]
                                .detail[index]['code']
                                .toString()
                                .toUpperCase(),
                            style: GoogleFonts.poppins(
                                letterSpacing: 1,
                                color: Colors.grey,
                                fontSize: 13),
                          ),

                          //Category product and weight
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              //Category product
                              Text(
                                allProducts[selectedIndex].detail[index]
                                    ['category'],
                                style: GoogleFonts.poppins(
                                    color: Colors.grey, fontSize: 13),
                              ),

                              //Weight
                              Text(
                                allProducts[selectedIndex]
                                    .detail[index]['weight']
                                    .toString()
                                    .toUpperCase(),
                                style: GoogleFonts.poppins(
                                    letterSpacing: 1,
                                    color: AppTheme.nearlyBlack,
                                    fontSize: 16),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                )),
          );
        },

            //childCount, the amount of GridView box
            childCount: allProducts[selectedIndex].detail.length),
      ),
    );
  }
}
