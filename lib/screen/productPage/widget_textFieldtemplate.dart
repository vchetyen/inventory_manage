import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

String _textInTextField;

class TextFieldFormWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final EdgeInsetsGeometry padding;
  TextFieldFormWidget(
      {this.hintText, this.textEditingController, this.padding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: TextField(
        controller: textEditingController,
        onChanged: (value) {
          _textInTextField = value;
          print(_textInTextField);
        },
        decoration: InputDecoration(
            suffix: IconButton(
              padding: const EdgeInsets.all(0),
              icon: Icon(Icons.clear),
              onPressed: () {
                textEditingController.clear();
              },
              color: Colors.grey,
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: AppTheme.mainColor),
            ),
            hintText: hintText,
            hintStyle: GoogleFonts.poppins(fontSize: 17)),
      ),
    );
  }
}
