//Category List for ListView.builder in Product page
var categoryList = [
  'ALL',
  'Main Ingredient',
  'Coffee',
  'Tea',
  'Milk',
  'Coffee bean'
];

//Class template data for each product
class Product {
  final dynamic category, detail;
  Product({this.category, this.detail});
}

//List of Product class
List<Product> allProducts = [
  //All product tab
  Product(
    category: 'All',
    detail: [
      {
        'name': 'mi1',
        'category': 'Main Ingredient',
        'code': '00mi1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '130'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '140'
              },
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '130'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '140'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:45 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '09:00 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
            ]
          },
          {
            'date': '04 Oct 2020',
            'detail': [
              {
                'time': '11:45 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '09:00 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
            ]
          },
        ]
      },
      {
        'name': 'c1',
        'category': 'Coffee',
        'code': '00c1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
        ]
      },
      {
        'name': 't1',
        'category': 'Tea',
        'code': '00t1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/t1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm1',
        'category': 'Milk',
        'code': '00m1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb1',
        'category': 'Coffee bean',
        'code': '00cb1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '8:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'mi2',
        'category': 'Main Ingredient',
        'code': '00mi2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'mi3',
        'category': 'Main Ingredient',
        'code': '00mi3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'c2',
        'category': 'Coffee',
        'code': '00c2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'c3',
        'category': 'Coffee',
        'code': '00c3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 't2',
        'category': 'Tea',
        'code': '00t2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/t2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm2',
        'category': 'Milk',
        'code': '00m2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm3',
        'category': 'Milk',
        'code': '00m3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb2',
        'category': 'Coffee bean',
        'code': '00cb2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb3',
        'category': 'Coffee bean',
        'code': '00cb3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'mi4',
        'category': 'Main Ingredient',
        'code': '00mi4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm4',
        'category': 'Milk',
        'code': '00m4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb4',
        'category': 'Coffee bean',
        'code': '00cb4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb5',
        'category': 'Coffee bean',
        'code': '00cb5',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb5.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),
  //Main Ingredient tab
  Product(
    category: 'Main Ingredient',
    detail: [
      {
        'name': 'mi1',
        'category': 'Main Ingredient',
        'code': '00mi1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '130'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '140'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:45 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '09:00 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
            ]
          },
        ]
      },
      {
        'name': 'mi2',
        'category': 'Main Ingredient',
        'code': '00mi2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterdaysss',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'mi3',
        'category': 'Main Ingredient',
        'code': '00mi3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'mi4',
        'category': 'Main Ingredient',
        'code': '00mi4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/mi4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),

  //Coffee Tab
  Product(
    category: 'Coffee',
    detail: [
      {
        'name': 'c1',
        'category': 'Coffee',
        'code': '00c1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          },
        ]
      },
      {
        'name': 'c2',
        'category': 'Coffee',
        'code': '00c2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'c3',
        'category': 'Coffee',
        'code': '00c3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/c3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),

  //Tea tab
  Product(
    category: 'Tea',
    detail: [
      {
        'name': 't1',
        'category': 'Tea',
        'code': '00t1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/t1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-20',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 't2',
        'category': 'Tea',
        'code': '00t2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/t2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),

  //Milk tab
  Product(
    category: 'Milk',
    detail: [
      {
        'name': 'm1',
        'category': 'Milk',
        'code': '00m1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm2',
        'category': 'Milk',
        'code': '00m2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm3',
        'category': 'Milk',
        'code': '00m3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'm4',
        'category': 'Milk',
        'code': '00m4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/m4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),

  //Coffee bean tab
  Product(
    category: 'Coffee bean',
    detail: [
      {
        'name': 'cb1',
        'category': 'Coffee bean',
        'code': '00cb1',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb1.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb2',
        'category': 'Coffee bean',
        'code': '00cb2',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb2.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb3',
        'category': 'Coffee bean',
        'code': '00cb3',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb3.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb4',
        'category': 'Coffee bean',
        'code': '00cb4',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb4.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
      {
        'name': 'cb5',
        'category': 'Coffee bean',
        'code': '00cb5',
        'weight': '1kg',
        'pic': 'assets/images/productPage/cb5.jpg',
        'transactionHistory': [
          {
            'date': 'Today',
            'detail': [
              {
                'time': '12:41 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '12:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+60',
                'qtyOnHand': '90'
              },
            ]
          },
          {
            'date': 'Yesterday',
            'detail': [
              {
                'time': '11:01 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-30',
                'qtyOnHand': '130'
              },
              {
                'time': '9:30 am',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '160'
              },
              {
                'time': '7:03 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+50',
                'qtyOnHand': '150'
              },
            ]
          },
          {
            'date': 'Thu 1st Oct 2020',
            'detail': [
              {
                'time': '04:30 pm',
                'stockInOrOut': 'Stock in',
                'amountOfAddOrMinus': '+10',
                'qtyOnHand': '100'
              },
              {
                'time': '1:30 pm',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-10',
                'qtyOnHand': '90'
              },
              {
                'time': '8:14 am',
                'stockInOrOut': 'Stock out',
                'amountOfAddOrMinus': '-50',
                'qtyOnHand': '100'
              },
            ]
          }
        ]
      },
    ],
  ),
];
