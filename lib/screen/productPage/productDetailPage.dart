import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/productPage/dataProductPage.dart';
import 'package:inventory_manage/screen/productPage/widget_listViewBuilderTransactionHistory.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ProductDetailPage extends StatefulWidget {
  final int index, selectedIndex;
  ProductDetailPage({this.index, this.selectedIndex});
  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: AppTheme.backGround,
        body: Stack(
          children: [
            CustomScrollView(slivers: [
              SliverToBoxAdapter(
                  //Column
                  //Inside it, there are Product details, bgImage,Transaction History
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Container that has background img and product detail
                  Container(
                      width: size.width,
                      height: size.height * 0.40,
                      //background image
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage(
                                  allProducts[widget.selectedIndex]
                                      .detail[widget.index]['pic']))),
                      //Column that wraps Black opacity background Container Product Detail
                      child: Column(
                        children: [
                          Spacer(), //push Black opacity background Container to the bottom
                          //Black opacity background Container
                          Container(
                            margin: const EdgeInsets.only(top: 50),
                            height: size.height * 0.22,
                            width: size.width,
                            color: Colors.black.withOpacity(0.5),
                            //Padding for all texts inside Black opacity background Container
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              child: FittedBox(
                                //To fit everything inside Black opacity background Container
                                //Use it to prevent overflow
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    //name and category product
                                    Text(
                                        (allProducts[widget.selectedIndex]
                                                .detail[widget.index]['name']
                                                .toString()
                                                .toUpperCase() +
                                            " " +
                                            "(${allProducts[widget.selectedIndex].detail[widget.index]['category']})"),
                                        style: GoogleFonts.poppins(
                                            letterSpacing: 1,
                                            color: Colors.white,
                                            fontSize: 23,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    //barcode product
                                    Text(
                                        'Barcode: ' +
                                            '${allProducts[widget.selectedIndex].detail[widget.index]['code']}',
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 20)),
                                    //price product
                                    Text('Price: ',
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 20)),
                                    //Type product
                                    Text('Type: Stock',
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 20)),
                                    //Quanity product
                                    Text(
                                        'Quantity: ' +
                                            '${allProducts[widget.selectedIndex].detail[widget.index]['weight']}',
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 20)),
                                    //Latest transaction
                                    Text('Latest transactions: ' + 'Today 6pm',
                                        style: GoogleFonts.poppins(
                                            color: Colors.white, fontSize: 20)),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                ],
              )),
              SliverPadding(
                padding: const EdgeInsets.only(left: 10, top: 20, bottom: 20),
                sliver: SliverToBoxAdapter(
                  child: Text(
                    'Transaction History',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold,
                        color: AppTheme.nearlyBlack,
                        fontSize: 18),
                  ),
                ),
              ),
              ListViewTransactionHistory(
                selectedIndex: widget.selectedIndex,
                index: widget.index,
              ),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: 20,
                ),
              )
            ]),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                        maxRadius: 23,
                        backgroundColor: Colors.grey.shade100.withOpacity(0.6),
                        child: IconButton(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onPressed: () {
                            Navigator.pop(context);
                            print('back to product page');
                          },
                          icon: Icon(
                            MdiIcons.arrowLeft,
                            color: Colors.black,
                          ),
                        )),
                    CircleAvatar(
                        maxRadius: 23,
                        backgroundColor: Colors.grey.shade100.withOpacity(0.6),
                        child: IconButton(
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          onPressed: () {
                            print('3 dots');
                          },
                          icon: Icon(
                            Icons.more_vert,
                            color: Colors.black,
                          ),
                        )),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
