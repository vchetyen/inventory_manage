import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

import 'dataProductPage.dart';

class AlertDialogWidget {
  final int indexx, index, selectedIndex, indexs;
  AlertDialogWidget({this.indexx, this.index, this.selectedIndex, this.indexs});

  //Custom dialog template
  void alertDialogWidget(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    //For showing dialog template
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogBox(
            selectedIndex: selectedIndex,
            width: width,
            height: height,
            index: index,
            indexs: indexs,
            indexx: indexx,
          );
        });
  }
}

//Refactor widget || widget template
//It is dialogbox which is used inside showDialog
class DialogBox extends StatelessWidget {
  final int selectedIndex, index, indexs, indexx;
  const DialogBox({
    Key key,
    @required this.width,
    @required this.height,
    @required this.selectedIndex,
    @required this.index,
    @required this.indexs,
    @required this.indexx,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: FittedBox(
        child: UnconstrainedBox(
          child: Material(
            color: AppTheme.backGround,
            borderRadius: BorderRadius.circular(10),
            //Inside
            child: Container(
              padding: const EdgeInsets.all(20),
              width: width,
              height: height / 2,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    top: -60,
                    left: width / 2 - 50,
                    child: CircleAvatar(
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(width: 2, color: Colors.white)),
                      ),
                      maxRadius: 40,
                      backgroundImage: AssetImage(
                          allProducts[selectedIndex].detail[index]['pic']),
                    ),
                  ),
                  FittedBox(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        //Name+Caetgory
                        Text(
                          allProducts[selectedIndex]
                                  .detail[index]['name']
                                  .toString()
                                  .toUpperCase() +
                              " " +
                              "(${allProducts[selectedIndex].detail[index]['category']})",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold, fontSize: 23),
                        ),

                        //Barcode
                        Text(
                            'Barcode: ' +
                                '${allProducts[selectedIndex].detail[index]['code']}',
                            style: GoogleFonts.poppins(fontSize: 17)),
                        SizedBox(
                          height: 40,
                        ),
                        //Transaction Type: Stock in Or Stock out
                        Row(
                          children: [
                            Text('Transaction Type',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['stockInOrOut'],
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            )
                          ],
                        ),
                        //Transaction Date:
                        Row(
                          children: [
                            Text('Transaction Date',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              // '1st Oct 2020',
                              allProducts[selectedIndex].detail[index]
                                  ['transactionHistory'][indexs]['date'],
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            ),
                            Text(" "),
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['time'],
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            ),
                          ],
                        ),

                        //User
                        Row(
                          children: [
                            Text('User',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              "Soknoi",
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            )
                          ],
                        ),
                        //Option
                        Row(
                          children: [
                            Text('Options',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              "Used",
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            )
                          ],
                        ),
                        //Amount
                        Row(
                          children: [
                            Text('Amount',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['amountOfAddOrMinus'],
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            )
                          ],
                        ),
                        //Qty on hand
                        Row(
                          children: [
                            Text('Qty On Hand',
                                style: GoogleFonts.poppins(
                                    fontSize: 22, fontWeight: FontWeight.bold)),
                            Text(
                              ":",
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(width: 20),
                            Text(
                              allProducts[selectedIndex].detail[index]
                                      ['transactionHistory'][indexs]['detail']
                                  [indexx]['qtyOnHand'],
                              style: GoogleFonts.poppins(
                                  color: Colors.grey, fontSize: 22),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
