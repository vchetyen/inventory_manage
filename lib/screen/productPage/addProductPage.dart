import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/productPage/dataProductPage.dart';
import 'camera.dart';
import 'widget_textFieldtemplate.dart';

class AddProductPage extends StatefulWidget {
  @override
  _AddProductPageState createState() => _AddProductPageState();
}

class _AddProductPageState extends State<AddProductPage> {
  TextEditingController _proNameController = TextEditingController();
  TextEditingController _proBarCodeController = TextEditingController();
  TextEditingController _proCostController = TextEditingController();
  TextEditingController _proPriceController = TextEditingController();
  TextEditingController _proUnitController = TextEditingController();
  TextEditingController _proCateController = TextEditingController();
  TextEditingController _proDescController = TextEditingController();
  //TextEditingController _dropDownTextController = TextEditingController();

  //Value for dropdown menu
  String _currentSelectedValue;

  String image = '';
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      body: CustomScrollView(
        slivers: [
          //AppBar
          SliverAppBar(
            elevation: 0,
            pinned: true,
            backgroundColor: AppTheme.backGround,
            //ArrowBack Icon
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            centerTitle: true,
            title: Text("ADD PRODUCT",
                style: GoogleFonts.poppins(color: AppTheme.mainColor)),
            //Check Icon
            actions: [
              IconButton(
                padding: const EdgeInsets.only(right: 20),
                onPressed: () {},
                icon: Icon(
                  Icons.check,
                  color: Colors.black,
                  size: 22,
                ),
              ),
            ],
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 20,
            ),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //Camera
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: [
                      // camera for add product
                      Container(
                        width: 100,
                        height: 140,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                                width: 1, color: AppTheme.nearlyBlack)),
                        child: IconButton(
                          icon: Icon(Icons.camera_alt),
                          onPressed: () async {
                            var cam = await Navigator.push(context,
                                CupertinoPageRoute(
                                    builder: (BuildContext context) {
                              return Camera();
                            }));
                            print(cam);
                            if (cam != null) {
                              setState(() {
                                image = cam;
                              });
                            }
                          },
                        ),
                        //Camera and photo
                        // child: ListView.builder(
                        //   scrollDirection: Axis.horizontal,
                        //   itemCount: 2,
                        //   itemBuilder: ((_, index) {
                        //     return index == 0
                        //         ? InkWell(
                        //             splashColor: Colors.transparent,
                        //             highlightColor: Colors.transparent,
                        //             onTap: () {
                        //               return getImage();
                        //             },
                        //             //Camera container
                        //             child: Container(
                        //               margin: const EdgeInsets.only(left: 20),
                        //               width: 100,
                        //               decoration: BoxDecoration(
                        //                   border: Border.all(
                        //                       width: 1, color: AppTheme.nearlyBlack),
                        //                   borderRadius: BorderRadius.circular(8)),
                        //               child: Icon(
                        //                 Icons.camera_alt,
                        //                 color: AppTheme.nearlyBlack,
                        //               ),
                        //             ),
                        //           )
                        //         //Photo container
                        //         : Container(
                        //             margin: const EdgeInsets.only(left: 10),
                        //             width: 100,
                        //             child: _image == null
                        //                 ? null
                        //                 : Image.file(_image, fit: BoxFit.cover));
                        //   }),
                        // ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      // photo
                      Container(
                        width: 100,
                        height: 140,
                        decoration: BoxDecoration(
                        ),
                        child: Image.file(
                          File(image),
                          fit: BoxFit.cover,
                        ),
                        //Camera and photo
                        // child: ListView.builder(
                        //   scrollDirection: Axis.horizontal,
                        //   itemCount: 2,
                        //   itemBuilder: ((_, index) {
                        //     return index == 0
                        //         ? InkWell(
                        //             splashColor: Colors.transparent,
                        //             highlightColor: Colors.transparent,
                        //             onTap: () {
                        //               return getImage();
                        //             },
                        //             //Camera container
                        //             child: Container(
                        //               margin: const EdgeInsets.only(left: 20),
                        //               width: 100,
                        //               decoration: BoxDecoration(
                        //                   border: Border.all(
                        //                       width: 1, color: AppTheme.nearlyBlack),
                        //                   borderRadius: BorderRadius.circular(8)),
                        //               child: Icon(
                        //                 Icons.camera_alt,
                        //                 color: AppTheme.nearlyBlack,
                        //               ),
                        //             ),
                        //           )
                        //         //Photo container
                        //         : Container(
                        //             margin: const EdgeInsets.only(left: 10),
                        //             width: 100,
                        //             child: _image == null
                        //                 ? null
                        //                 : Image.file(_image, fit: BoxFit.cover));
                        //   }),
                        // ),
                      ),
                    ],
                  ),
                ),

                TextFieldFormWidget(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  textEditingController: _proNameController,
                  hintText: 'Product Name',
                ),
                TextFieldFormWidget(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  textEditingController: _proBarCodeController,
                  hintText: 'Product Barcode',
                ),

                //Cost Price Unit textfields
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(children: [
                    Expanded(
                      child: TextFieldFormWidget(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        textEditingController: _proCostController,
                        hintText: 'Cost',
                      ),
                    ),
                    Expanded(
                      child: TextFieldFormWidget(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        textEditingController: _proPriceController,
                        hintText: 'Price',
                      ),
                    ),
                    Expanded(
                      child: TextFieldFormWidget(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        textEditingController: _proUnitController,
                        hintText: 'Unit',
                      ),
                    ),
                  ]),
                ),

                //Dropdown menu
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: FormField(
                    builder: (formFieldSate) {
                      return InputDecorator(
                        decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppTheme.mainColor),
                        )),
                        child: DropdownButton(
                          items: categoryList
                              .map((String dropSelectItemAdd) =>
                                  DropdownMenuItem(
                                    value: dropSelectItemAdd,
                                    child: Text(dropSelectItemAdd),
                                  ))
                              .toList(),
                          onChanged: (String value) {
                            setState(() {
                              _currentSelectedValue = value;
                            });
                          },
                          value: _currentSelectedValue,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: AppTheme.spacer,
                          elevation: 0,
                          underline: Container(
                              height: 1.0,
                              decoration: const BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0)))),
                          hint: Text(
                            "Category",
                            style: GoogleFonts.poppins(fontSize: 16.0),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                TextFieldFormWidget(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  textEditingController: _proDescController,
                  hintText: 'Descriptions',
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Future getBytes() async {
    Uint8List bytes = File(image).readAsBytesSync() as Uint8List;
    return ByteData.view(bytes.buffer);
  }
}
