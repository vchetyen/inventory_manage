import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/screen/productPage/addProductPage.dart';
import 'package:inventory_manage/screen/productPage/widget_sliverGridProductPage.dart';
import '../../model/theme.dart';
import 'dataProductPage.dart';
import 'widget_delegate.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

//-----------------------------------------------------------//
//start from this
int _selectedIndex =
    0; //Index for selecting ListView builder and also SliverGridView

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      //FloatingAction button
      //Add product (+) button
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 30, right: 10),
        child: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: AppTheme.nearlyBlack,
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) {
              return AddProductPage();
            }));
            print('go to Add product page');
          },
          backgroundColor: AppTheme.mainColor,
        ),
      ),

      //Body
      body: CustomScrollView(
        slivers: [
          //AppBar
          SliverAppBar(
            elevation: 0,
            pinned: true,
            //Back icon on the left
            leading: IconButton(
              padding: const EdgeInsets.only(left: 15),
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                print('back to homescreen');
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios,
                  size: 20, color: AppTheme.nearlyBlack),
            ),
            //Search icon on the right
            actions: [
              IconButton(
                padding: const EdgeInsets.only(right: 15),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () {
                  print('search product');
                },
                icon: Icon(Icons.search, size: 25, color: AppTheme.nearlyBlack),
              ),
            ],
            backgroundColor: AppTheme.backGround,
            title: Text('PRODUCTS',
                style: GoogleFonts.poppins(
                    color: AppTheme.mainColor, fontSize: 22)),
            centerTitle: true,
          ),

          //ListView category
          //I use SliverPersistenHeader for pinning it and put ListView.builder inside of it instead of using SilverList
          SliverPersistentHeader(
            pinned: true,
            delegate: Delegate(
              maxHeight: 36,
              minHeight: 36,
              child: Container(
                padding: const EdgeInsets.only(left: 15),
                color: AppTheme.backGround,
                //ListView.bulilder for Category tab
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: categoryList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          _selectedIndex =
                              index; //set _selectIndex = index of ListView.builder category
                        });
                        print(
                            'SelectedIndex:$_selectedIndex ${categoryList[_selectedIndex]}');
                      },

                      //Each box of ListView.builder category
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          horizontal: 5,
                        ), //Margin left and right from one category box to other category box
                        padding: const EdgeInsets.symmetric(
                            horizontal:
                                25), //Padding left and right inside each category box
                        decoration: BoxDecoration(
                            color: _selectedIndex == index
                                ? AppTheme.mainColor
                                : Colors
                                    .white, //background color for each category box
                            borderRadius: BorderRadius.circular(50)),

                        child: Center(
                            //Center text in each Category box
                            child: Text(categoryList[index],
                                style: GoogleFonts.poppins(
                                    //Color for each categorybox
                                    //If we click on the box, its bgcolor will be MainColor or else,White
                                    color: _selectedIndex == index
                                        ? Colors.white
                                        : Colors.black,
                                    fontSize:
                                        16) //fontSize of text inside categorybox

                                )),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),

          //Row under Category ListView.builder contains number of amount of product on the left and Filter/Sort icon on the right
          //I use SliverPersistentHeader for pinning it
          SliverPersistentHeader(
              pinned: true,

              //I use custom Delegate again which I made it in other file.
              delegate: Delegate(
                maxHeight: 50,
                minHeight: 50,
                child: Container(
                  color: AppTheme.backGround,
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 10,
                    top: 20,
                  ),
                  height: 50,
                  child: Row(
                    //Make space in middle between left Text and right Filter/Sort Icon
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //Amount of product Text
                      Text(
                        (allProducts[_selectedIndex].detail).length.toString() +
                            ' ' +
                            'Products',
                        style: GoogleFonts.poppins(
                            color: Colors.black, fontSize: 16),
                      ),

                      //Sort/Filter icon
                      IconButton(
                          padding: EdgeInsets.all(0),
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          icon: Icon(Icons.sort),
                          iconSize: 24,
                          onPressed: () {
                            print('Filter item');
                          })
                    ],
                  ),
                ),
              )),

          //Custom SliverGridView product
          //I made its template on the other file. Click on it to see
          SliverGridProductPage(
            selectedIndex: _selectedIndex,
          )
        ],
      ),
    );
  }
}
