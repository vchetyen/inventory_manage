import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/inventory/fieldItemScanProduct.dart';

class AddStockForm extends StatefulWidget {
  @override
  _AddStockFormState createState() => _AddStockFormState();
}

class _AddStockFormState extends State<AddStockForm> {
  final _formKey = GlobalKey<FormState>();
  final currentIndex = 0;
  // Item for Dropdown
  var _currencies = ['Soda', 'Coffee', 'Tea', 'Apple'];
  // dropDown Value in Add Stock page for Vendor List
  String dwVendorValue;
  // dropDown Value in Remove Stock page for Option List
  String dwOptionValue;

  String sAddPro = "";
  String getsAddPro = "";

  Future scanbarAddPro() async {
    getsAddPro = await FlutterBarcodeScanner.scanBarcode(
        "#e7a071", "cancel", true, ScanMode.DEFAULT);
    setState(() {
      sAddPro = getsAddPro;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        color: AppTheme.backGround,
        key: ValueKey(1),
        child: Form(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FormField(
                    builder: (FormFieldState) {
                      return InputDecorator(
                        // just want only one border bottom line
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.nearlyBlack.withOpacity(0.2)),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppTheme.nearlyBlack.withOpacity(0.2)),
                          ),
                        ),
                        child: FieldItemScanPro(
                          ptext: sAddPro == '' ? 'Choose Product' : sAddPro,
                          picon: Icon(
                            Icons.crop_free,
                            size: 22,
                          ),
                          press: () {
                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (_) => ScanPage()));
                            scanbarAddPro();
                          },
                        ),
                      );
                    },
                  ),
                ),
                // form dropdown value
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FormField(
                    builder: (FormFieldState) {
                      return InputDecorator(
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            )),
                        child: DropdownButton(
                          items: _currencies
                              .map((String dropSelectItem) => DropdownMenuItem(
                                    value: dropSelectItem,
                                    child: Text(dropSelectItem),
                                  ))
                              .toList(),
                          onChanged: (String value) {
                            setState(() {
                              dwVendorValue = value;
                            });
                          },
                          value: dwVendorValue,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: AppTheme.spacer,
                          elevation: 0,
                          underline: Container(
                              height: 1.0,
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0)))),
                          hint: Text("Vendor",
                              style: GoogleFonts.poppins(fontSize: 16.0)),
                        ),
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Enter some QTY Please';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: 'QTY',
                      hintStyle: GoogleFonts.poppins(fontSize: 16.0),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Enter some Cost Please';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: 'Cost',
                      hintStyle: GoogleFonts.poppins(fontSize: 16.0),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Enter some Description Please';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: 'Description',
                      hintStyle: GoogleFonts.poppins(fontSize: 16.0),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: AppTheme.nearlyBlack.withOpacity(0.2)),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
