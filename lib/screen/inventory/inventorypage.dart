import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/inventory/formCreate.dart';

import 'cardItemOnhand.dart';
import 'cardItemProducts.dart';
import 'circleTabIndicator.dart';

class InventoryPage extends StatefulWidget {
  @override
  _InventoryPageState createState() => _InventoryPageState();
}

class _InventoryPageState extends State<InventoryPage>
    with TickerProviderStateMixin {
  TabController _tabController;
  // i just create array list
  //  A server or save the information in a database.

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      // Button for create new inventory
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => FormCreateStock()));
        },
        child: Icon(
          Icons.add,
          color: AppTheme.backGround,
        ),
        tooltip: 'Add New Inventory',
        backgroundColor: AppTheme.mainColor,
      ),
      // To go scroll pages
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 22,
                color: AppTheme.nearlyBlack,
              ),
              onPressed: () => Navigator.pop(context),
            ),
            title: Text(
              "INVENTORY",
              // style: TextStyle(color: AppTheme.mainColor),
              style: GoogleFonts.poppins(
                  color: AppTheme.mainColor, fontSize: 18.0),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.search,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {},
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Tab(
                      child: Container(
                        color: AppTheme.backGround,
                        height: 50,
                        child: TabBar(
                          // labelPadding: EdgeInsets.symmetric(horizontal: 50),
                          controller: _tabController,
                          isScrollable: true,
                          unselectedLabelColor:
                              AppTheme.nearlyBlack.withOpacity(0.3),
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                          labelColor: AppTheme.nearlyBlack,
                          indicator: CircleTabIndicator(
                              color: AppTheme.mainColor, radius: 3),
                          tabs: [
                            Text(
                              "Today",
                              style: GoogleFonts.poppins(fontSize: 16),
                            ),
                            Text(
                              "On Hand",
                              style: GoogleFonts.poppins(fontSize: 16),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: size.height * 0.75,
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        //Inventory Items
                        //Inventory Today list details
                        _buildInvToday(size),
                        // Inventory list detail on hand
                        _buildInvOhHand(size)
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  ListView _buildInvToday(Size size) {
    List<Widget> item = [
      CardItemProducts(
        time: "12:45 pm",
        title: "Product Name",
        cost: '100',
      ),
      CardItemProducts(
        time: "12:45 pm",
        title: "Product Name",
        cost: '200',
      ),
      CardItemProducts(
        time: "12:45 pm",
        title: "Product Name",
        cost: '220',
      )
    ];
    return ListView.builder(
        itemCount: item.length,
        padding: EdgeInsets.all(0),
        scrollDirection: Axis.vertical,
        itemBuilder: (_, index) {
          return Padding(
              padding: const EdgeInsets.all(8.0),
              // new widget for have some details in inventoryItems
              child: item[index]);
        });
  }
}

ListView _buildInvOhHand(Size size) {
  List<Widget> itemOhhand = [
    CardItemOnhand(
      time: '12:55 am',
      title: 'Product Name',
      cost: '200',
    ),
    CardItemOnhand(
      time: '12:55 am',
      title: 'Product Name',
      cost: '88',
    ),
    CardItemOnhand(
      time: '12:55 am',
      title: 'Product Name',
      cost: '101',
    )
  ];
  return ListView.builder(
      itemCount: itemOhhand.length,
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      itemBuilder: (_, index) {
        return Padding(
            padding: const EdgeInsets.all(8.0),
            // new widget for have some details in inventoryItems
            child: itemOhhand[index]);
      });
}
