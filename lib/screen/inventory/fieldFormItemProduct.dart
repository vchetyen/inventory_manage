import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class FiledFormItem extends StatefulWidget {
  final String validatortext;
  final String text;
  const FiledFormItem({
    Key key,
    this.text, this.validatortext,
  }) : super(key: key);


  @override
  _FiledFormItemState createState() => _FiledFormItemState();
}

class _FiledFormItemState extends State<FiledFormItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return widget.validatortext;
          }
          return null;
        },
        decoration: InputDecoration(
            hintText: widget.text,
          hintStyle: GoogleFonts.poppins(fontSize: 16.0),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),),
      ),
    );
  }
}