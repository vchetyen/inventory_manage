import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class FieldItemScanPro extends StatefulWidget {
  final Widget picon;
  final String ptext;
  final Function press;
  const FieldItemScanPro({
    Key key,
    this.picon,
    this.ptext,
    this.press,
  }) : super(key: key);

  @override
  _FieldItemScanProState createState() => _FieldItemScanProState();
}

class _FieldItemScanProState extends State<FieldItemScanPro> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.ptext,
          // style: TextStyle(
          //     fontSize: 17.0, color: AppTheme.nearlyBlack.withOpacity(0.5)),
          style: GoogleFonts.poppins(
              fontSize: 16.0, color: AppTheme.nearlyBlack.withOpacity(0.6)),
        ),
        IconButton(
          icon: widget.picon,
          onPressed: widget.press,
        )
      ],
    );
  }
}
