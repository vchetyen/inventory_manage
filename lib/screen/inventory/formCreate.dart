import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/inventory/addStock.dart';
import 'package:inventory_manage/screen/inventory/fieldFormItemProduct.dart';
import 'package:inventory_manage/screen/inventory/fieldItemScanProduct.dart';
import 'package:inventory_manage/screen/inventory/removeStock.dart';

class FormCreateStock extends StatefulWidget {
  @override
  _FormCreateStockState createState() => _FormCreateStockState();
}

class _FormCreateStockState extends State<FormCreateStock>
    with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  TabController _tabController;
  List<Widget> pages = [];
  int _currentTabIdx = 0;
  final currentIndex = 0;
  List<Widget> list = [
    Text(
      "In",
      style: TextStyle(
        fontSize: 16.0,
      ),
    ),
    Text(
      "Out",
      style: TextStyle(
        fontSize: 16.0,
      ),
    )
  ];

  @override
  void initState() {
    _tabController =
        TabController(vsync: this, length: list.length, initialIndex: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    pages = [
      // page add stock
      AddStockForm(),
      // page remove stock
      RemoveStockForm()
    ];
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      body: CustomScrollView(
        physics: NeverScrollableScrollPhysics(),
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                size: 22,
                color: AppTheme.nearlyBlack,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            // i just want to switch title of appbar when change pages
            title: currentIndex == _currentTabIdx
                ? Text(
                    "ADD STOCK",
                    // style: TextStyle(color: AppTheme.mainColor,fontFamily: AppTheme.fontName),
                    style: GoogleFonts.poppins(
                        color: AppTheme.mainColor, fontSize: 18.0),
                  )
                : Text(
                    "REMOVE STOCK",
                    style: GoogleFonts.poppins(
                        color: AppTheme.mainColor, fontSize: 18.0),
                  ),
            centerTitle: true,
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.check,
                    size: 22,
                    color: AppTheme.nearlyBlack,
                  ),
                onPressed: () {
                  // Validate returns true if the form is valid, otherwise false.
                  // if (_formKey.currentState.validate()) {
                  //   // If the form is valid, display a snackbar. In the real world,
                  //   // you'd often call a server or save the information in a database.
                  //
                  //   Scaffold.of(context)
                  //       .showSnackBar(SnackBar(content: Text('Processing Data')));
                  // }
                },
                  )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              width: size.width,
              height: size.height,
              color: AppTheme.backGround,
              child: Stack(
                children: [
                  // is packages transition when switch page
                  PageTransitionSwitcher(
                      duration: Duration(milliseconds: 50),
                      reverse: true,
                      transitionBuilder:
                          (child, primaryAnimation, secondaryAnimation) =>
                              FadeThroughTransition(
                                animation: primaryAnimation,
                                secondaryAnimation: secondaryAnimation,
                                child: child,
                              ),
                      child: pages[_currentTabIdx]),
                  Positioned(
                    left: 22,
                    top: 431,
                    // is button for switch pages
                    child: Container(
                      width: 118,
                      height: 30,
                      child: TabBar(
                          isScrollable: false,
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              color: AppTheme.nearlyBlack,
                              border: Border.all(
                                  width: 2,
                                  color: AppTheme.mainColor.withOpacity(0.3))),
                          onTap: (idx) {
                            setState(() {
                              currentIndex == _currentTabIdx;
                              this._currentTabIdx = idx;
                            });
                          },
                          controller: _tabController,
                          tabs: list),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
