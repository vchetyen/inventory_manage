import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

import 'fieldFormItemProduct.dart';
import 'fieldItemScanProduct.dart';

class RemoveStockForm extends StatefulWidget {
  @override
  _RemoveStockFormState createState() => _RemoveStockFormState();
}

class _RemoveStockFormState extends State<RemoveStockForm> {
  final _formKey = GlobalKey<FormState>();
  final currentIndex = 0;
  // Item for Dropdown
  var _currencies = ['Soda', 'Coffee', 'Tea', 'Apple'];
  // dropDown Value in Add Stock page for Vendor List
  String dwVendorValue;
  // dropDown Value in Remove Stock page for Option List
  String dwOptionValue;

  String sRemovePro = "";
  String getsRemovePro = "";

  Future scanbarRmPro() async {
    getsRemovePro = await FlutterBarcodeScanner.scanBarcode(
        "#e7a071", "cancel", true, ScanMode.DEFAULT);
    setState(() {
      sRemovePro = getsRemovePro;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: AppTheme.backGround,
        width: size.width,
        key: ValueKey(2),
        child: Form(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FormField(
                    builder: (FormFieldState) {
                      return InputDecorator(
                        // just want only one border bottom line
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            )),
                        child: FieldItemScanPro(
                          ptext:
                              sRemovePro == '' ? 'Choose Product' : sRemovePro,
                          picon: Icon(
                            Icons.crop_free,
                            size: 22,
                          ),
                          press: () {
                            scanbarRmPro();
                          },
                        ),
                      );
                    },
                  ),
                ),
                // form dropdown value
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FormField(
                    builder: (FormFieldState) {
                      return InputDecorator(
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppTheme.nearlyBlack.withOpacity(0.2)),
                            )),
                        child: DropdownButton(
                          items: _currencies
                              .map((String dropSelectItemAdd) =>
                                  DropdownMenuItem(
                                    value: dropSelectItemAdd,
                                    child: Text(dropSelectItemAdd),
                                  ))
                              .toList(),
                          onChanged: (String value) {
                            setState(() {
                              dwOptionValue = value;
                            });
                          },
                          value: dwOptionValue,
                          isExpanded: true,
                          isDense: true,
                          dropdownColor: AppTheme.spacer,
                          elevation: 0,
                          underline: Container(
                              height: 1.0,
                              decoration: const BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.transparent,
                                          width: 0.0)))),
                          hint: Text(
                            "Options",
                            style: GoogleFonts.poppins(fontSize: 16.0),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                // form check QTYqd
                FiledFormItem(
                  text: 'QTY',
                  validatortext: 'Enter some QTY Please',
                ),
                FiledFormItem(
                  text: 'Price',
                  validatortext: 'Enter some Price Please',
                ),
                FiledFormItem(
                  text: 'Description',
                  validatortext: 'enter some Description Please',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
