import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class CardItemOnhand extends StatefulWidget {
  final String title;
  final String time;
  final String cost;
  const CardItemOnhand({
    Key key, this.title, this.time, this.cost,
  }) : super(key: key);

  @override
  _CardItemOnhandState createState() => _CardItemOnhandState();
}

class _CardItemOnhandState extends State<CardItemOnhand> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375,
      height: 55,
      decoration: BoxDecoration(
        // need only border on bottm list items
          border: Border(
              bottom: BorderSide(
                  width: 1,
                  color: AppTheme.nearlyBlack.withOpacity(0.2)))),
      child: ListTile(
        leading: Text(
          widget.time,
          style: GoogleFonts.poppins(
              color: AppTheme.nearlyBlack.withOpacity(0.5),
              fontSize: 10.0),
        ),
        title: Text(
          widget.title,
          style: GoogleFonts.poppins(
              color: AppTheme.nearlyBlack, fontSize: 16.0),
        ),
        trailing: Container(
          width: 56.8,
          height: 23,
          decoration: BoxDecoration(
              color: AppTheme.greenBackground,
              borderRadius: BorderRadius.circular(35.0)),
          child: Center(
              child: Text(
                widget.cost,
                // style: TextStyle(color: AppTheme.greenColor),
                style: GoogleFonts.poppins(
                    color: AppTheme.greenColor,
                    fontSize: 13.0,
                    fontWeight: FontWeight.bold),
              )),
        ),
      ),
    );
  }
}