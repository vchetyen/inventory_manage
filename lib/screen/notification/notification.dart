import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/notificationitems.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/notification/lowstock.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppTheme.backGround,
        centerTitle: true,
        title: Text(
          "NOFICATIONS",
          style: GoogleFonts.poppins(color: AppTheme.mainColor),
        ),
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: AppTheme.nearlyBlack,
          ),
        ),
      ),
      body: Container(
        //Body Notification
        width: size.width,
        height: size.height,
        color: AppTheme.backGround,
        padding: EdgeInsets.only(top: 20, left: 10),
        child: ListView(
          children: [
            titleNotification("New"),
            stockinstockNotification("Nita Remove Coffee From Stock", "-10",
                "12:00", Colors.red, context),
            Divider(
              color: AppTheme.nearlyBlack,
            ),
            stockinstockNotification("Nita Add Coffee To Stock", "+10", "02:00",
                Colors.green, context),
            Divider(
              color: AppTheme.nearlyBlack,
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                titleNotification("Low Stock Alert"),
                Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: InkWell(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SeeAllLowStock())),
                      child: Hero(
                        tag: 'shownotifica',
                        child: Material(
                          type: MaterialType.transparency,
                          child: Text(
                            "See all",
                            style: GoogleFonts.poppins(
                                fontSize: 16,
                                color: Colors.black.withOpacity(0.5),
                                letterSpacing: 1.0),
                          ),
                        ),
                      ),
                    )),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            stockalertNotification("Coffee Nearly out of stocks.", "12:00pm",
                "assets/images/productPage/cb5.jpg", context),
            Divider(
              color: Colors.grey,
            ),
            stockalertNotification("Syrup nearly out of stocks.", "12:00pm",
                "assets/images/productPage/m2.jpg", context),
            Divider(
              color: AppTheme.nearlyBlack,
            ),
            SizedBox(
              height: 20,
            ),
            titleNotification("Earlier"),
            stockinstockNotification("Nita removed Coffee from Stock", "-10",
                "12:00pm", Colors.red, context),
            Divider(
              color: AppTheme.nearlyBlack,
            ),
            stockinstockNotification("Nita add Coffee from Stock", "-10",
                "12:00pm", Colors.green, context),
            Divider(
              color: AppTheme.nearlyBlack,
            )
          ],
        ),
      ),
    );
  }
}
