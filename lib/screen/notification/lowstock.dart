import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/notificationitems.dart';
import 'package:inventory_manage/model/theme.dart';

class SeeAllLowStock extends StatefulWidget {
  @override
  _SeeAllLowStockState createState() => _SeeAllLowStockState();
}

class _SeeAllLowStockState extends State<SeeAllLowStock> {
  List<String> nameObject = [
    'Coffee',
    'Syrub',
    'Sugar',
    'Milk Powder',
    'Cocoa Powder',
    'Jelly',
    'Yugurt',
    'Cups',
    'Fresh Milk',
    'Green Tea',
    'Red Tea',
    'Ovantine',
    'Honey',
    'Condensed Milk'
  ];
  List<String> texts = [
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks",
    " Nearly out of stocks"
  ];
  List<String> times = [
    "11:00pm",
    "12:00pm",
    "01:00pm",
    "02:00pm",
    "03:00pm",
    "04:00pm",
    "05:00pm",
    "07:00am",
    "09:00am",
    "10:00am",
    "11:00am",
    "08:00am",
    "11:00am",
    "08:00am",
  ];
  List<String> images = [
    "assets/images/productPage/cb5.jpg",
    "assets/images/productPage/syrub.jpg",
    "assets/images/productPage/sugar.jpg",
    "assets/images/productPage/milkpowder.jpg",
    "assets/images/productPage/cocoapowder.jpg",
    "assets/images/productPage/jelly.jpg",
    "assets/images/productPage/yugurt.jpg",
    "assets/images/productPage/cup.jpg",
    "assets/images/productPage/freshmilk.jpg",
    "assets/images/productPage/greentea.jpg",
    "assets/images/productPage/redtea.jpg",
    "assets/images/productPage/ovaltine.jpg",
    "assets/images/productPage/honey.jpg",
    "assets/images/productPage/condensedmilk.jpg",
  ];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      appBar: AppBar(
        backgroundColor: AppTheme.backGround,
        centerTitle: true,
        leading: Text(""),
        elevation: 0,
        title: Text(
          "NOTIFICATION",
          style: GoogleFonts.poppins(color: AppTheme.mainColor),
        ),
        actions: [
          Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 20),
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Hero(
                  tag: 'shownotifica',
                  child: Material(
                    type: MaterialType.transparency,
                    child: Text("Hide",
                        style: GoogleFonts.poppins(
                            color: Colors.grey, fontSize: 16)),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
      body: Column(
        children: [
          //Title notification
          Container(
            width: size.width,
            height: 50,
            padding: EdgeInsets.only(left: 10),
            child: titleNotification("Low Stock Alert"),
          ),
          Expanded(
            child: Container(
              width: size.width,
              height: size.height,
              child: ListView.builder(
                itemCount: 14,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      stockalertNotification(
                          "${(nameObject[index] + (texts[index] + (texts[index])))}",
                          times[index],
                          images[index],
                          context),
                      Divider(
                        color: AppTheme.nearlyBlack,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
