import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/drawerItems.dart';
import 'package:inventory_manage/model/imicons.dart';
import 'package:inventory_manage/model/profile.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/Expenses/expensesPage.dart';
import 'package:inventory_manage/screen/category/categoryPage.dart';
import 'package:inventory_manage/screen/inventory/addStock.dart';
import 'package:inventory_manage/screen/inventory/formCreate.dart';
import 'package:inventory_manage/screen/inventory/inventorypage.dart';
import 'package:inventory_manage/screen/inventory/removeStock.dart';
import 'package:inventory_manage/screen/notification/notification.dart';
import 'package:inventory_manage/screen/productPage/productPage.dart';
import 'package:inventory_manage/screen/profitLoss/profitLossPage.dart';
import 'package:inventory_manage/screen/report/reportPage.dart';
import 'package:inventory_manage/screen/scan/ScanPage.dart';
import 'package:inventory_manage/screen/vendor/vendorPage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  //Animation IconMenu Drawer
  Animation animationIconMenuDrawer;
  AnimationController iConMenuController; // Controller Icon Menu Drawer
  //Handle Onpress IconMenuDrawer
  bool onCheckDrawer = false;

  // scan bar code
  String scan = "";
  String getScan = "";


  Future scanbarRmPro() async {
    getScan = await FlutterBarcodeScanner.scanBarcode(
        "#e7a071", "cancel", true, ScanMode.DEFAULT);
    setState(() {
      scan = getScan;
    });
  }

  @override
  void initState() {
    super.initState();
    //Controller Icon Menu Drawer
    iConMenuController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    animationIconMenuDrawer =
        CurvedAnimation(curve: Curves.fastOutSlowIn, parent: iConMenuController)
          ..addListener(() {
            setState(() {});
          });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Transform(
          transform: Matrix4.translationValues(
              size.width / 1.5 * animationIconMenuDrawer.value, 0, 0),
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: InkWell(
                onTap: () => Drawer(),
                child: IconButton(
                    onPressed: () {
                      setState(() {
                        animationIconMenuDrawer.value == 0
                            ? iConMenuController.forward(from: 0.0)
                            : iConMenuController.reverse(from: 1.0);
                      });
                    },
                    icon: Icon(
                      Icons.menu,
                      color: AppTheme.mainColor,
                    )),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: InkWell(
                    onTap: () {
                      print("Notification");
                      Navigator.push(
                          context,
                          CupertinoPageRoute(
                              builder: (context) => NotificationPage()));
                    },
                    child: Icon(
                      Icons.notifications,
                      color: AppTheme.mainColor,
                      size: 30,
                    ),
                  ),
                )
              ],
              title: Text("INVENTORY MANAGEMENT",
                  style: GoogleFonts.poppins(
                      color: AppTheme.mainColor, fontSize: 18)),
            ),

            //Body HomePage

            body: Container(
              width: size.width,

              height: size.height,

              color: Colors.white,

              padding: EdgeInsets.only(left: 17, right: 16),

              //Widgets in in body

              child: Column(
                children: [
                  //Profile UI
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Container(
                      width: 342,
                      height: 80,
                      padding: EdgeInsets.only(
                        left: 20,
                      ),
                      child: Row(
                        children: [
                          //Profile Pricture
                          ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: Container(
                              width: 60,
                              height: 60,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/profile.jpg"))),
                            ),
                          ),

                          //Profile Name

                          Padding(
                            padding: EdgeInsets.only(top: 15, left: 40),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                //nAme

                                Text("Mai Khalifa",
                                    style: GoogleFonts.poppins(
                                        color: AppTheme.nearlyBlack,
                                        fontSize: 16)),

                                Text("Manager",
                                    style: GoogleFonts.poppins(
                                        color: Colors.grey, fontSize: 14))
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  //Menu home page
                  Container(
                    width: size.width,
                    height: size.height / 3,
                    child: Table(
                      border: TableBorder.symmetric(
                          inside: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      )),
                      children: [
                        TableRow(children: [
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                radius: 500,
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (_) => ProductPage()));
                                },
                                child: Padding(
                                  padding: EdgeInsets.only(top: 30),
                                  child: Column(
                                    children: [
                                      Icon(
                                        IMicons.product,
                                        size: 40,
                                        color: AppTheme.mainColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10, left: 10),
                                        child: Text(
                                          "Product",
                                          style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              color: AppTheme.mainColor),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) => ReportPage())),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 30),
                                  child: Column(
                                    children: [
                                      Icon(
                                        IMicons.reports,
                                        size: 40,
                                        color: AppTheme.mainColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Text(
                                          "Report",
                                          style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              color: AppTheme.mainColor),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) => InventoryPage())),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 25),
                                  child: Column(
                                    children: [
                                      Icon(
                                        IMicons.inventory,
                                        size: 55,
                                        color: AppTheme.mainColor,
                                      ),
                                      Text(
                                        "Iventory",
                                        style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            color: AppTheme.mainColor),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) => ExpensesPage())),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 35),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        IMicons.expense,
                                        size: 40,
                                        color: AppTheme.mainColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10, bottom: 20),
                                        child: Text(
                                          "Expenses",
                                          style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              color: AppTheme.mainColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                // onTap: () => Navigator.push(
                                //     context,
                                //     CupertinoPageRoute(
                                //         builder: (context) => ScanPage())),
                                onTap: () => scanbarRmPro(),
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 35),
                                  child: Column(
                                    children: [
                                      Icon(
                                        IMicons.scan,
                                        size: 40,
                                        color: AppTheme.mainColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Text(
                                          "Scan",
                                          style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              color: AppTheme.mainColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                        builder: (context) => ProfitPage())),
                                child: Padding(
                                  padding: EdgeInsets.only(top: 35),
                                  child: Column(
                                    children: [
                                      Icon(
                                        IMicons.profit,
                                        size: 35,
                                        color: AppTheme.mainColor,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 15),
                                        child: Text(
                                          "Profit Loss",
                                          style: GoogleFonts.poppins(
                                              fontSize: 12,
                                              color: AppTheme.mainColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ])
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  //Stock in
                  InkWell(
                    //OnTab Stock In
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (_) => FormCreateStock()));
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 18, right: 17),
                      width: 340,
                      height: 100,
                      decoration: BoxDecoration(
                          color: AppTheme.mainColor,
                          borderRadius: BorderRadius.circular(12.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          //Add Stock Button
                          ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: Container(
                              width: 60,
                              height: 60,
                              color: Colors.white,
                              child: Center(
                                  child: Icon(
                                MdiIcons.plus,
                                size: 50,
                                color: AppTheme.mainColor,
                              )),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left: 55),
                            child: Text(
                              "Stock In",
                              style: GoogleFonts.poppins(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  //Stock Out
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: InkWell(
                      //Ontap Stock out
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => FormCreateStock()));
                      },
                      child: Container(
                        padding: EdgeInsets.only(right: 17, left: 18),
                        width: 340,
                        height: 100,
                        decoration: BoxDecoration(
                            color: AppTheme.mainColor,
                            borderRadius: BorderRadius.circular(12.0)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            //Add Stock Button
                            ClipRRect(
                              borderRadius: BorderRadius.circular(30.0),
                              child: Container(
                                width: 60,
                                height: 60,
                                color: Colors.white,
                                child: Center(
                                    child: Icon(
                                  MdiIcons.minus,
                                  size: 50,
                                  color: AppTheme.mainColor,
                                )),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 50),
                              child: Text(
                                "Stock Out",
                                style: GoogleFonts.poppins(
                                    fontSize: 18,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        //Drawer Builder
        Transform(
          transform: Matrix4.translationValues(
              -size.width + (size.width * animationIconMenuDrawer.value), 0, 0),
          child: Material(
            child: Container(
                width: size.width / 1.5,
                height: size.height,
                color: AppTheme.nearlyBlack,
                child: ListView(
                  children: [
                    ProfileAccount(
                      imageProfile: "assets/images/profile.jpg",
                      accountName: "Mai Khalifa",
                      position: "Manager",
                    ),
                    Divider(
                      height: 1,
                      endIndent: 10,
                      indent: 10,
                      color: AppTheme.mainColor,
                    ),
                    //Vender
                    createDrawerItems(
                      icon: MdiIcons.accountMultiple,
                      text: "Vendors",
                      onTap: () {
                        print("Vendors");
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => VendorPage()));
                      },
                    ),
                    //Category
                    createDrawerItems(
                      icon: MdiIcons.shapePlus,
                      text: "Category",
                      onTap: () {
                        print("Category");
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (context) => CategoryPage()));
                      },
                    ),
                    //Setting
                    createDrawerItems(
                      icon: Icons.settings,
                      text: "Setting",
                      onTap: () {
                        print("Setting");
                      },
                    ),
                    SizedBox(
                      height: size.height / 3,
                    ),
                    Divider(
                      endIndent: 10,
                      indent: 10,
                      color: AppTheme.mainColor,
                    ),
                    //Logout
                    createDrawerItems(
                      icon: MdiIcons.logoutVariant,
                      text: "Logout",
                      onTap: () {
                        print("Logout");
                      },
                    )
                  ],
                )),
          ),
        )
      ],
    );
  }
}
