import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/homepage/homepage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //username controller
  var uNameController = TextEditingController();
  //userPassword controller
  var uPasswordController = TextEditingController();
  String username = "admin";
  String upassword = "admin";
  String query = "";
  bool isName = false;
  bool isPass = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        children: [
          Container(
            width: size.width,
            height: size.height,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            padding: EdgeInsets.only(top: 40, left: 38, right: 37),
            child: Column(
              children: [
                //loGo login
                Container(
                  width: 300,
                  height: 300,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/images/logo3.png"))),
                ),
                //uSername textfield
                TextField(
                  onChanged: (val) => setState(() {
                    query = val;
                    query.toString() == username
                        ? isName = true
                        : isName = false;
                    print("Username Is $isName");
                  }),
                  controller: uNameController,
                  decoration: InputDecoration(
                      hintText: "Username",
                      hintStyle: TextStyle(letterSpacing: 1.0),
                      suffix: IconButton(
                        onPressed: () => uNameController.clear(),
                        icon: Icon(Icons.clear),
                      )),
                ),

                //uSerPassword textfield
                TextField(
                  onChanged: (val) => setState(() {
                    query = val;
                    query.toString() == upassword
                        ? isPass = true
                        : isPass = false;
                    print("Password is $isPass");
                  }),
                  obscureText: true,
                  controller: uPasswordController,
                  decoration: InputDecoration(
                      hintText: "Password",
                      hintStyle: TextStyle(
                        letterSpacing: 1.0,
                      ),
                      suffix: IconButton(
                        onPressed: () => uPasswordController.clear(),
                        icon: Icon(Icons.clear),
                      )),
                ),

                //Login Button
                Padding(
                  padding: EdgeInsets.only(top: 50, right: 0, left: 0),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        isName && isPass == true
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyHomePage()))
                            : Alert(
                                context: context,
                                type: AlertType.warning,
                                title: "Your Passowrd or Username is invalid!",
                                buttons: [
                                    DialogButton(
                                      color: AppTheme.mainColor,
                                      child: Text(
                                        "Try Again",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    )
                                  ]).show();
                      });
                    },
                    child: Container(
                      width: 150,
                      height: 50,
                      decoration: BoxDecoration(
                          color: AppTheme.mainColor,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Center(
                        child: Text(
                          "Login",
                          style: GoogleFonts.poppins(
                              fontSize: 17, color: AppTheme.backGround),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
