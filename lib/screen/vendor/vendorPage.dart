import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/vendor/formAddVendor.dart';
import 'package:inventory_manage/screen/vendor/vendorListItems.dart';

class VendorPage extends StatefulWidget {
  @override
  _VendorPageState createState() => _VendorPageState();
}

class _VendorPageState extends State<VendorPage> {
  List<Widget> vendorItem = [
    // Widget Vendor detail
    VendorListItems(),
    VendorListItems(),
    VendorListItems()
  ];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppTheme.mainColor,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => FormAddVendor()));
        },
        child: Icon(
          Icons.add,
          color: AppTheme.spacer,
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppTheme.nearlyBlack,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "VENDORS",
              style: GoogleFonts.poppins(color: AppTheme.mainColor),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.search,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {},
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
                height: size.height,
                child: ListView.builder(
                    itemCount: vendorItem.length,
                    scrollDirection: Axis.vertical,
                    padding: EdgeInsets.all(0.0),
                    itemBuilder: (_, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 5),
                        child: vendorItem[index],
                      );
                    })),
          )
        ],
      ),
    );
  }
}
