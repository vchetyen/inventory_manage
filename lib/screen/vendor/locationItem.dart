import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class LocationItems extends StatefulWidget {
  final String text;
  final Widget icon;
  const LocationItems({
    Key key,
    this.text,
    this.icon,
  }) : super(key: key);

  @override
  _LocationItemsState createState() => _LocationItemsState();
}

class _LocationItemsState extends State<LocationItems> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          widget.icon,
          SizedBox(
            width: 10,
          ),
          Container(
            width: 130,
            height: 32,
            child: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                widget.text,
                overflow: TextOverflow.ellipsis,
                textDirection: TextDirection.ltr,
                maxLines: 2,
                style: GoogleFonts.ubuntu(color: AppTheme.spacer, fontSize: 13),
              ),
            ),
          )
        ],
      ),
    );
  }
}
