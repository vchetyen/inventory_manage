import 'package:flutter/material.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/vendor/contactItem.dart';
import 'package:inventory_manage/screen/vendor/locationItem.dart';

class VendorListItems extends StatefulWidget {
  const VendorListItems({
    Key key,
  }) : super(key: key);

  @override
  _VendorListItemsState createState() => _VendorListItemsState();
}

class _VendorListItemsState extends State<VendorListItems> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 142,
      decoration: BoxDecoration(
          color: AppTheme.nearlyBlack,
          borderRadius: BorderRadius.circular(4.0)),
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 99,
                  height: 57,
                  child: Text(
                    "Kofi",
                    style: TextStyle(
                        fontSize: 51,
                        color: AppTheme.mainColor,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                // for vendor details i just create new widget for easy check
                // is part of widget in vendor detail for check contact
                ContactItems(
                  img: "assets/images/icons8-facebook.png",
                  text: 'Kofi',
                ),
                ContactItems(
                  img: "assets/images/unnamed.png",
                  text: 'xxx xxx xxx',
                ),
                ContactItems(
                  img: "assets/images/phone-16.png",
                  text: '(+855) 12 345 678',
                ),
              ],
            ),
          ),
          //location
          Padding(
            padding: const EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  width: 200,
                  child: Column(
                    children: [
                      // for vendor details i just create new widget for easy check
                      // is part of widget in vendor detail for check find
                      LocationItems(
                        icon: Icon(
                          Icons.location_on,
                          size: 22,
                          color: AppTheme.mainColor,
                        ),
                        text: 'Phnom Penh',
                      ),
                      LocationItems(
                        icon: Icon(
                          Icons.access_time,
                          size: 22,
                          color: AppTheme.mainColor,
                        ),
                        text: '5 Days',
                      ),
                      LocationItems(
                        icon: Icon(
                          Icons.widgets,
                          size: 22,
                          color: AppTheme.mainColor,
                        ),
                        text: 'COFFEE , GLASSES, GLASSES',
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
