import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/vendor/cameraScreen.dart';
import 'package:inventory_manage/screen/vendor/listItemAddVendor.dart';

class FormAddVendor extends StatefulWidget {
  @override
  _FormAddVendorState createState() => _FormAddVendorState();
}

class _FormAddVendorState extends State<FormAddVendor> {
  final _formKey = GlobalKey<FormState>();
  String image = '';
  final currentIndex = 0;
  var _currencies = ['Soda', 'Coffee', 'Tea', 'Apple'];
  String dropDownValue;

  int selectIndex = 0;
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppTheme.nearlyBlack,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "ADD VENDORS",
              style: GoogleFonts.poppins(color: AppTheme.mainColor),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.check,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {
                  // Validate returns true if the form is valid, otherwise false.
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.

                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Processing Data')));
                  }
                },
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
              child: Column(
                // Image picker
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        // camera for add product
                        Container(
                          width: 100,
                          height: 140,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(
                                  width: 1, color: AppTheme.nearlyBlack)),
                          child: IconButton(
                            icon: Icon(Icons.camera_alt),
                            onPressed: () async {
                              var cam = await Navigator.push(context,
                                  CupertinoPageRoute(
                                      builder: (BuildContext context) {
                                        return CameraScreen();
                                      }));
                              print(cam);
                              if (cam != null) {
                                setState(() {
                                  image = cam;
                                });
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        // photo
                        Container(
                          width: 100,
                          height: 140,
                          decoration: BoxDecoration(
                          ),
                          child: Image.file(
                            File(image),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        //is widget detail check vendor name for item list add vendor
                        ListItemAddVendor(
                          title: 'Vendor Name',
                          validatortext: 'Please enter vendor name!!',
                        ),
                        // list product supply with dropdown products
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FormField(
                            builder: (FormFieldState) {
                              return InputDecorator(
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppTheme.nearlyBlack
                                              .withOpacity(0.2)),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppTheme.nearlyBlack
                                              .withOpacity(0.2)),
                                    )),
                                child: DropdownButton(
                                  items: _currencies
                                      .map((String dropSelectItem) =>
                                          DropdownMenuItem(
                                            value: dropSelectItem,
                                            child: Text(dropSelectItem),
                                          ))
                                      .toList(),
                                  onChanged: (String value) {
                                    setState(() {
                                      dropDownValue = value;
                                    });
                                  },
                                  value: dropDownValue,
                                  isExpanded: true,
                                  isDense: true,
                                  dropdownColor: AppTheme.spacer,
                                  elevation: 0,
                                  underline: Container(
                                      height: 1.0,
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.transparent,
                                                  width: 0.0)))),
                                  hint: Text("Product Supply",
                                      style:
                                          GoogleFonts.poppins(fontSize: 16.0)),
                                ),
                              );
                            },
                          ),
                        ),
                        // for widget to avoid this kind of issue with TextField
                        // need 2 expanded for easy
                        Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Delivery time!!';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      hintText: 'Delivery Time',
                                      hintStyle:
                                          GoogleFonts.poppins(fontSize: 16.0),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppTheme.nearlyBlack
                                                .withOpacity(0.2)),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppTheme.nearlyBlack
                                                .withOpacity(0.2)),
                                      )),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Address!!';
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                      hintText: 'Address',
                                      hintStyle:
                                          GoogleFonts.poppins(fontSize: 16.0),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppTheme.nearlyBlack
                                                .withOpacity(0.2)),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppTheme.nearlyBlack
                                                .withOpacity(0.2)),
                                      )),
                                ),
                              ),
                            ),
                          ],
                        ),
                        //is widget detail check number phone for item list add vendor
                        ListItemAddVendor(title: 'Tel',validatortext: 'Please enter Telephone!!',),
                        //is widget detail check facebook account for item list add vendor
                        ListItemAddVendor(title: 'Facebook',validatortext: 'Please enter Facebook account!!',),
                        //is widget detail check ABA account in item list add vendor
                        ListItemAddVendor(title: 'ABA Account',validatortext: 'Please enter ABA account!!',),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
