import 'package:flutter/material.dart';
import 'package:inventory_manage/model/theme.dart';

class ContactItems extends StatefulWidget {
  final String img;
  final String text;
  const ContactItems({
    Key key,
    this.img,
    this.text,
  }) : super(key: key);

  @override
  _ContactItemsState createState() => _ContactItemsState();
}

class _ContactItemsState extends State<ContactItems> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 10.8,
          height: 10.9,
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover, image: AssetImage(widget.img))),
        ),
        SizedBox(
          width: 5.6,
        ),
        Text(
          widget.text,
          style: TextStyle(fontSize: 13.0, color: AppTheme.spacer),
        )
      ],
    );
  }
}
