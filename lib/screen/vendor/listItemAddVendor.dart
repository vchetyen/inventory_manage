import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class ListItemAddVendor extends StatefulWidget {
  final String title;
  final String validatortext;
  const ListItemAddVendor({
    Key key,
    this.title, this.validatortext,
  }) : super(key: key);

  @override
  _ListItemAddVendorState createState() => _ListItemAddVendorState();
}

class _ListItemAddVendorState extends State<ListItemAddVendor> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return widget.validatortext;
          }
          return null;
        },
        decoration: InputDecoration(
            hintText: widget.title,
            enabledBorder: UnderlineInputBorder(
              borderSide:
              BorderSide(color: AppTheme.nearlyBlack.withOpacity(0.2)),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide:
              BorderSide(color: AppTheme.nearlyBlack.withOpacity(0.2)),
            ),
            hintStyle: GoogleFonts.poppins(
              fontSize: 16.0,
            )),
      ),
    );
  }
}