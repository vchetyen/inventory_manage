import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/Expenses/fieldItemList.dart';

class FormExpenses extends StatefulWidget {
  @override
  _FormExpensesState createState() => _FormExpensesState();
}

// Create a global key that uniquely identifies the Form widget
// and allows validation of the form.
final _formKey = GlobalKey<FormState>();

class _FormExpensesState extends State<FormExpenses> {
  var textcontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      appBar: AppBar(
        backgroundColor: AppTheme.backGround,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 22,
            color: AppTheme.nearlyBlack,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "ADD EXPENSES",
          // style: TextStyle(color: AppTheme.mainColor,fontFamily: AppTheme.fontName),
          style: GoogleFonts.poppins(
            color: AppTheme.mainColor,
            fontSize: 18.0,
          ),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.check,
              size: 22,
              color: AppTheme.nearlyBlack,
            ),
            onPressed: () {
              // Validate returns true if the form is valid, otherwise false.
              if (_formKey.currentState.validate()) {
                // If the form is valid, display a snackbar. In the real world,
                // you'd often call a server or save the information in a database.

                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Processing Data')));
              }
            },
          )
        ],
      ),
      body: Container(
        width: size.width,
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
            child: Column(
              children: [
                FieldItemList(
                  text: 'Expenses Title',
                  validatortext: 'Please enter some the Title',
                ),
                FieldItemList(
                  text: 'Expenses Description',
                  validatortext: 'Please enter some Description',
                ),
                FieldItemList(
                  text: 'Expenses Amount',
                  validatortext: 'Please enter some Amount',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
