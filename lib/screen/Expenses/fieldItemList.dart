import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class FieldItemList extends StatefulWidget {
  final String validatortext;
  final String text;
  const FieldItemList({
    Key key,
    this.text,
    this.validatortext,
  }) : super(key: key);

  @override
  _FieldItemListState createState() => _FieldItemListState();
}

class _FieldItemListState extends State<FieldItemList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return widget.validatortext;
          }
          return null;
        },
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
                color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),
          hintText: widget.text,
          hintStyle: GoogleFonts.poppins(
            color: AppTheme.nearlyBlack.withOpacity(0.5),
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }
}
