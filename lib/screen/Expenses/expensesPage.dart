import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/Expenses/formCreateIncomes.dart';
import 'package:inventory_manage/screen/Expenses/formcreateExpenses.dart';

import 'cardItemList.dart';

class ExpensesPage extends StatefulWidget {
  @override
  _ExpensesPageState createState() => _ExpensesPageState();
}

class _ExpensesPageState extends State<ExpensesPage> {
  PageController _pageController;
  int _page = 0;
  @override
  void initState() {
    super.initState();
    _pageController = PageController(keepPage: true, initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      appBar: AppBar(
        backgroundColor: AppTheme.backGround,
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: 22,
            color: AppTheme.nearlyBlack,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: _page == 0
            ? Text(
                "EXPENSES",
                // style: TextStyle(color: AppTheme.mainColor,fontFamily: AppTheme.fontName),
                style: GoogleFonts.poppins(
                    color: AppTheme.mainColor, fontSize: 18.0),
              )
            : Text(
                "INCOMES",
                // style: TextStyle(color: AppTheme.mainColor,fontFamily: AppTheme.fontName),
                style: GoogleFonts.poppins(
                    color: AppTheme.mainColor, fontSize: 18.0),
              ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              size: 22,
              color: AppTheme.nearlyBlack,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: (int page) {
          setState(() {
            _page = page;
          });
        },
        children: [
          // expenses pages
          _buildExpensesPage(size),
          // incomes pages
          _buildIncomePage(size)
        ],
      ),
    );
  }

  // build expenses method
  Container _buildExpensesPage(Size size) {
    List<Widget> itemExpenses = [
      CardItemList(
        title: 'Fixing',
        date: '10/10/2020',
        color: AppTheme.nearlyRed.withOpacity(0.2),
        numcolor: AppTheme.nearlyRed, // expenses number
      ),
      CardItemList(
        title: 'Fixing',
        date: '10/10/2020',
        color: AppTheme.nearlyRed.withOpacity(0.2),
        numcolor: AppTheme.nearlyRed, // expenses number
      ),
      CardItemList(
        title: 'Fixing',
        date: '10/10/2020',
        color: AppTheme.nearlyRed.withOpacity(0.2),
        numcolor: AppTheme.nearlyRed, // expenses number
      )
    ];
    return Container(
      width: size.width,
      height: size.height,
      child: Stack(
        children: [
          // cardItemlist
          Container(
            height: size.height * 0.8,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "4 Expenses",
                        // style: TextStyle(fontSize: 16.0,fontFamily: AppTheme.fontName),
                        style: GoogleFonts.poppins(fontSize: 14.0),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.sort,
                          size: 18.0,
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      itemCount: itemExpenses.length,
                      itemBuilder: (_, index) {
                        return itemExpenses[index];
                      }),
                )
              ],
            ),
          ),
          // Amount Total
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: size.width,
              height: 40,
              color: AppTheme.secondaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total",
                      // style:
                      // TextStyle(fontSize: 16, fontWeight: FontWeight.bold,fontFamily: AppTheme.fontName),
                      style: GoogleFonts.poppins(
                          color: AppTheme.nearlyBlack,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "100\$",
                      // style: TextStyle(
                      //     fontSize: 18.0, fontWeight: FontWeight.bold,),
                      style: GoogleFonts.poppins(
                          color: AppTheme.nearlyBlack,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
          // button create
          Positioned(
            right: 15,
            bottom: 45,
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => FormExpenses()));
              },
              child: Container(
                width: 55,
                height: 60,
                decoration: BoxDecoration(
                    color: AppTheme.mainColor, shape: BoxShape.circle),
                child: Icon(
                  Icons.add,
                  color: AppTheme.spacer,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  //build income method
  Container _buildIncomePage(Size size) {
    List<Widget> itemIncome = [
      CardItemList(
        title: 'Daily Sold',
        date: '10/10/2020',
        color: AppTheme.greenBackground,
        numcolor: AppTheme.greenColor, //incomes number
      ),
      CardItemList(
        title: 'Daily Sold',
        date: '10/10/2020',
        color: AppTheme.greenBackground,
        numcolor: AppTheme.greenColor, //incomes number
      ),
      CardItemList(
        title: 'Daily Sold',
        date: '10/10/2020',
        color: AppTheme.greenBackground,
        numcolor: AppTheme.greenColor, //incomes number
      )
    ];
    return Container(
      width: size.width,
      height: size.height,
      child: Stack(
        children: [
          // cardItemlist
          Container(
            height: size.height * 0.8,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "4 Incomes",
                        style: TextStyle(fontSize: 14.0),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.sort,
                          size: 18.0,
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      itemCount: itemIncome.length,
                      itemBuilder: (_, index) {
                        return itemIncome[index];
                      }),
                )
              ],
            ),
          ),
          // Amount Total
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: size.width,
              height: 40,
              color: AppTheme.secondaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Total",
                      // style: TextStyle(
                      //     fontSize: 16.0, fontWeight: FontWeight.bold),
                      style: GoogleFonts.poppins(
                          color: AppTheme.nearlyBlack,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "100\$",
                      // style: TextStyle(
                      //     fontSize: 18.0, fontWeight: FontWeight.bold),
                      style: GoogleFonts.poppins(
                          color: AppTheme.nearlyBlack,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
          // button create
          Positioned(
            right: 15,
            bottom: 45,
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => FormIncomes()));
              },
              child: Container(
                width: 55,
                height: 60,
                decoration: BoxDecoration(
                    color: AppTheme.mainColor, shape: BoxShape.circle),
                child: Icon(
                  Icons.add,
                  color: AppTheme.spacer,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
