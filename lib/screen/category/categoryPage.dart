import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/category/formAddCategory.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: AppTheme.mainColor,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => FormAddCat()));
        },
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppTheme.nearlyBlack,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "CATEGORIES",
              style: GoogleFonts.poppins(color: AppTheme.mainColor),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.search,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {},
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              width: size.width,
              height: size.height,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 22),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "4 Categories",
                          // style: TextStyle(fontSize: 16.0,fontFamily: AppTheme.fontName),
                          style: GoogleFonts.poppins(fontSize: 14.0),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.sort,
                            size: 18.0,
                          ),
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                        padding: EdgeInsets.all(0),
                        scrollDirection: Axis.vertical,
                        itemCount: 4,
                        itemBuilder: (_, index) {
                          return Card(
                            shadowColor: AppTheme.nearlyBlack,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: RawMaterialButton(
                              onPressed: () {  },
                              child: Container(
                                width: 374,
                                height: 62,
                                child: ListTile(
                                  title: Text("Main Ingredient",
                                      // style: GoogleFonts.poppins(fontSize: 16.0),
                                      style: GoogleFonts.rubik()),
                                  subtitle: Text("10 Product Total",
                                      style: GoogleFonts.rubik(fontSize: 13.0)),
                                  trailing: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 20,
                                    color: AppTheme.nearlyBlack,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
