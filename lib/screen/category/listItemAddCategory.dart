import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class ListItemAddCat extends StatefulWidget {
  final String text;
  final String validatortext;
  const ListItemAddCat({
    Key key, this.text, this.validatortext
  }) : super(key: key);

  @override
  _ListItemAddCatState createState() => _ListItemAddCatState();
}

class _ListItemAddCatState extends State<ListItemAddCat> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return widget.validatortext;
          }
          return null;
        },
        decoration: InputDecoration(
          hintText: widget.text,
          hintStyle: GoogleFonts.poppins(fontSize: 16.0),
          enabledBorder: UnderlineInputBorder(
            borderSide:
            BorderSide(color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide:
            BorderSide(color: AppTheme.nearlyBlack.withOpacity(0.2)),
          ),
        ),
      ),
    );
  }
}