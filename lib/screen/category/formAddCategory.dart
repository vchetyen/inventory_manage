import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:inventory_manage/screen/category/listItemAddCategory.dart';

import 'cameraScreen.dart';

class FormAddCat extends StatefulWidget {
  @override
  _FormAddCatState createState() => _FormAddCatState();
}

class _FormAddCatState extends State<FormAddCat> {
  final _formKey = GlobalKey<FormState>();
  String image = '';
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppTheme.nearlyBlack,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "ADD CATEGORY",
              style: GoogleFonts.poppins(color: AppTheme.mainColor),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.check,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {
                  // Validate returns true if the form is valid, otherwise false.
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.

                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Processing Data')));
                  }
                },
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 20),
              child: Column(
                // Camera
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        // camera for add product
                        Container(
                          width: 100,
                          height: 140,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(
                                  width: 1, color: AppTheme.nearlyBlack)),
                          child: IconButton(
                            icon: Icon(Icons.camera_alt),
                            onPressed: () async {
                              var cam = await Navigator.push(context,
                                  CupertinoPageRoute(
                                      builder: (BuildContext context) {
                                        return CameraScreen();
                                      }));
                              print(cam);
                              if (cam != null) {
                                setState(() {
                                  image = cam;
                                });
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        // photo
                        Container(
                          width: 100,
                          height: 140,
                          decoration: BoxDecoration(
                          ),
                          child: Image.file(
                            File(image),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        ListItemAddCat(
                          text: 'Category Name',
                          validatortext: 'Please enter category name!!',
                        ),
                        ListItemAddCat(
                          text: 'Description',
                          validatortext: 'Please enter description!!',
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}


