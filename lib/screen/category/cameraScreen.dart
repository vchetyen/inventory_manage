import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class CameraScreen extends StatefulWidget {
  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  // gallery for choose images on the camera
  File image;
  File imageGallery;

  // camera package
  // Add three variables to the state class to store the CameraController and cameras list and select camera index
  CameraController controller;
  List cameras;
  int selectedCameraIndex;

  @override
  void initState() {
    super.initState();
    // To display the current output from the camera,
    // create a CameraController.
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIndex = 0;
        });
        _initCameraController(cameras[selectedCameraIndex]).then((void v) {});
      } else {
        print('No camera available');
      }
    }).catchError((err) {
      print('Error :${err.code}Error message : ${err.message}');
    });
  }

  //initialize the controller. This returns a Future.
  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);

    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller.value.hasError) {
        print('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }
    if (mounted) {
      setState(() {});
    }
  }

  // create icon can be switch to face camera for take
  IconData _getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return CupertinoIcons.switch_camera;
      case CameraLensDirection.front:
        return CupertinoIcons.switch_camera_solid;
      case CameraLensDirection.external:
        return Icons.camera;
      default:
        return Icons.device_unknown;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          controller == null || !controller.value.isInitialized
              ? CupertinoActivityIndicator()
              : AspectRatio(
            aspectRatio: 3.0 / 4.0,
            child: CameraPreview(controller),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 80,
              width: double.infinity,
              color: AppTheme.nearlyBlack,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  // gallery images
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: RawMaterialButton(
                          splashColor: AppTheme.nearlyBlack.withOpacity(0.2),
                          child: Icon(
                            Icons.image,
                            color: AppTheme.spacer,
                          ),
                          onPressed: () async {
                            imageGallery = await ImagePicker.pickImage(
                                source: ImageSource.gallery);
                            setState(() {
                              image = imageGallery;
                            });
                            Navigator.pop(context, image.path);
                          }),
                    ),
                  ),
                  // camera preview images
                  ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30.0)),
                      child: RawMaterialButton(
                          splashColor: AppTheme.nearlyBlack.withOpacity(0.2),
                          child: Icon(
                            Icons.camera,
                            size: 60,
                            color: AppTheme.spacer,
                          ),
                          onPressed: () async {
                            try {
                              final path = join(
                                  (await getTemporaryDirectory()).path,
                                  '${DateTime.now()}.png');
                              await controller.takePicture(path);
                              Navigator.pop(context, path);
                            } catch (e) {
                              _showCameraException(e);
                            }
                          }),
                    ),
                  ),
                  // widget for switch camera when want take face camera
                  _cameraToggle()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _cameraToggle() {
    if (cameras == null || cameras.isEmpty) {
      return Spacer();
    }
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    CameraLensDirection lensDirection = selectedCamera.lensDirection;
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0)
        ),
        child: RawMaterialButton(
            splashColor: AppTheme.nearlyBlack.withOpacity(0.2),
            child: Icon(
              _getCameraLensIcon(lensDirection),
              color: AppTheme.spacer,
            ),
            onPressed: () {
              selectedCameraIndex = selectedCameraIndex < cameras.length - 1
                  ? selectedCameraIndex + 1
                  : 0;
              CameraDescription selectedCamera = cameras[selectedCameraIndex];
              _initCameraController(selectedCamera);
            }
        ),
      ),
    );
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error:${e.code}\nError message : ${e.description}';
    print(errorText);
  }
}
