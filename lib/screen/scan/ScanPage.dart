// import 'package:flashlight/flashlight.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_qr_bar_scanner/qr_bar_scanner_camera.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:inventory_manage/model/theme.dart';
// import 'package:inventory_manage/screen/homepage/homepage.dart';
// import 'package:inventory_manage/screen/scan/linkScanPage.dart';
//
// class ScanPage extends StatefulWidget {
//   @override
//   _ScanPageState createState() => _ScanPageState();
// }
//
// class _ScanPageState extends State<ScanPage> {
//   bool _hasFlashlight = false; //to set is there any flashlight ?
//   bool isturnon = false; //to set if flash light is on or off
//   IconData flashicon = Icons.flash_off; //icon for lashlight button
//
//   String _qrInfo = 'Scan a QR/Bar code';
//   bool _camState = false;
//
//   _qrCallback(String code) {
//     setState(() {
//       _camState = false;
//       _qrInfo = code;
//     });
//   }
//
//   _scanCode() {
//     setState(() {
//       _camState = true;
//     });
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     // scan code
//     _scanCode();
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     initFlashlight();
//   }
//
//   initFlashlight() async {
//     bool hasFlash = await Flashlight.hasFlashlight;
//     print("Device has flash ? $hasFlash");
//     setState(() {
//       _hasFlashlight = hasFlash;
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Scaffold(
//         backgroundColor: AppTheme.nearlyBlack,
//         body: _camState
//             ? ClipRRect(
//                 borderRadius: BorderRadius.circular(10),
//                 child: Stack(
//                   children: [
//                     SafeArea(
//                       child: Padding(
//                         padding: const EdgeInsets.symmetric(horizontal: 10),
//                         child: Stack(
//                           children: [
//                             InkWell(
//                               onTap: () {
//                                 // Navigator.pop(context);
//                                 Navigator.push(
//                                     context,
//                                     MaterialPageRoute(
//                                         builder: (_) => MyHomePage()));
//                               },
//                               child: Container(
//                                 width: 46,
//                                 height: 46,
//                                 decoration: BoxDecoration(
//                                     shape: BoxShape.circle,
//                                     color: AppTheme.spacer.withOpacity(0.5)),
//                                 child: Icon(
//                                   Icons.arrow_back_ios,
//                                   size: 22,
//                                   color: AppTheme.spacer,
//                                 ),
//                               ),
//                             ),
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.center,
//                               children: [
//                                 Container(
//                                   width: 100,
//                                   height: 46,
//                                   decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(30.0),
//                                       color: AppTheme.spacer.withOpacity(0.5)),
//                                   child: Center(
//                                       child: Text(
//                                     "SCAN",
//                                     style: GoogleFonts.poppins(
//                                         fontSize: 16.0, color: AppTheme.spacer),
//                                   )),
//                                 )
//                               ],
//                             ),
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.end,
//                               children: [
//                                 InkWell(
//                                   onTap: () {
//                                     if (isturnon) {
//                                       Flashlight.lightOn();
//                                       setState(() {
//                                         isturnon = false;
//                                         flashicon = Icons.flash_on;
//                                       });
//                                     } else {
//                                       Flashlight.lightOff();
//                                       setState(() {
//                                         isturnon = true;
//                                         flashicon = Icons.flash_off;
//                                       });
//                                     }
//                                   },
//                                   child: Container(
//                                       width: 46,
//                                       height: 46,
//                                       decoration: BoxDecoration(
//                                         shape: BoxShape.circle,
//                                         color: AppTheme.spacer.withOpacity(0.5),
//                                       ),
//                                       child: Icon(
//                                         flashicon,
//                                         color: AppTheme.spacer,
//                                       )),
//                                 )
//                               ],
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                     Container(
//                       width: size.width,
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Container(
//                             width: 250,
//                             height: 280,
//                             child: QRBarScannerCamera(
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                     border: Border.all(
//                                         width: 5, color: AppTheme.nearlyBlack),
//                                     borderRadius: BorderRadius.circular(10)),
//                               ),
//                               onError: (context, error) => Text(
//                                 error.toString(),
//                                 style: TextStyle(color: Colors.red),
//                               ),
//                               qrCodeCallback: (code) {
//                                 _qrCallback(code);
//                               },
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             : LinkScanPage(
//                 pageId: _qrInfo,
//               ));
//   }
// }
