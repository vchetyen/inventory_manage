import 'package:flutter/material.dart';
import 'package:inventory_manage/screen/scan/ScanPage.dart';

class LinkScanPage extends StatefulWidget {
  final String pageId;

  const LinkScanPage({Key key, this.pageId}) : super(key: key);
  @override
  _LinkScanPageState createState() => _LinkScanPageState();
}

class _LinkScanPageState extends State<LinkScanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (_) => ScanPage()));
          },
        ),
      ),
      body: Center(
        child: Text(widget.pageId),
      ),
    );
  }
}
