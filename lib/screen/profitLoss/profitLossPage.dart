import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:inventory_manage/screen/profitLoss/itemListCost.dart';
import 'package:inventory_manage/screen/profitLoss/itemListSale.dart';

import 'circleTabIndicator.dart';

class ProfitPage extends StatefulWidget {
  @override
  _ProfitPageState createState() => _ProfitPageState();
}

class _ProfitPageState extends State<ProfitPage> with TickerProviderStateMixin {
  TabController _tabController;
  int selectedIndex = 0;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this, initialIndex: 0);
  }

  final List<ProfitSeries> expenses = [
    // profitseries (int , String)
    ProfitSeries(500, 'July'),
    ProfitSeries(1800, 'Aug'),
    ProfitSeries(1500, 'Sep'),
    ProfitSeries(1000, 'Oct'),
    ProfitSeries(1200, 'Nov'),
    ProfitSeries(1000, 'Dec'),
  ];
  final List<ProfitSeries> incomes = [
    // profitseries (int , String)
    ProfitSeries(300, 'July'),
    ProfitSeries(1500, 'Aug'),
    ProfitSeries(2000, 'Sep'),
    ProfitSeries(800, 'Oct'),
    ProfitSeries(1000, 'Nov'),
    ProfitSeries(700, 'Dec'),
  ];
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // create list chart packages
    List<charts.Series<ProfitSeries, String>> series = [
      // charts for expenses
      charts.Series(
        id: 'Cost',
        domainFn: (ProfitSeries sales, _) => sales.month,
        measureFn: (ProfitSeries sales, _) => sales.profit,
        data: expenses,
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        fillColorFn: (_, __) => charts.MaterialPalette.green.shadeDefault.darker,
      ),
      // charts for incomes
      charts.Series(
        id: 'Price',
        domainFn: (ProfitSeries sales, _) => sales.month,
        measureFn: (ProfitSeries sales, _) => sales.profit,
        data: incomes,
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        fillColorFn: (_, __) => charts.MaterialPalette.deepOrange.shadeDefault,
      )
    ];
    return Scaffold(
      backgroundColor: AppTheme.backGround,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            backgroundColor: AppTheme.backGround,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppTheme.nearlyBlack,
                size: 22,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "PROFIT LOOS",
              style: GoogleFonts.poppins(color: AppTheme.mainColor),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.search,
                  size: 22,
                  color: AppTheme.nearlyBlack,
                ),
                onPressed: () {},
              )
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              width: size.width,
              height: size.height * 0.85,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 200,
                      child: charts.BarChart(
                        series,
                        animate: true,
                        defaultRenderer: charts.BarRendererConfig(
                            cornerStrategy: charts.ConstCornerStrategy(5),
                            groupingType: charts.BarGroupingType.grouped,
                            strokeWidthPx: 1.0),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Tab(
                      child: Container(
                        height: 50,
                        color: AppTheme.backGround,
                        child: TabBar(
                          controller: _tabController,
                          isScrollable: true,
                          labelPadding: EdgeInsets.symmetric(horizontal: 20),
                          unselectedLabelColor:
                              AppTheme.nearlyBlack.withOpacity(0.3),
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                          labelColor: AppTheme.nearlyBlack,
                          indicator: CircleTabIndicator(
                              color: AppTheme.mainColor, radius: 3),
                          tabs: [Text("All"), Text("Cost"), Text("Sale")],
                        ),
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          // all about expenses and incomes
                          Container(
                            child: ListView.builder(
                                padding: EdgeInsets.all(0.0),
                                physics: BouncingScrollPhysics(),
                                itemCount: 6,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (_, index) {
                                  return selectedIndex != index - 1
                                      ? ItemListCost(
                                          title: 'Cost of Fixing',
                                          date: '01-Oct-2020',
                                          color: AppTheme.nearlyRed
                                              .withOpacity(0.2),
                                          numcolor: AppTheme.nearlyRed,
                                        )
                                      : ItemListSale(
                                          title: 'Incomes',
                                          date: '02-Oct-2020',
                                          color: AppTheme.greenBackground,
                                          numcolor: AppTheme.greenColor,
                                        );
                                }),
                          ),
                          // make cost
                          Container(
                            child: ListView.builder(
                                padding: EdgeInsets.all(0.0),
                                physics: BouncingScrollPhysics(),
                                itemCount: 6,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (_, index) {
                                  return ItemListCost(
                                    title: 'Cost of Fixing',
                                    date: '01-Oct-2020',
                                    color: AppTheme.nearlyRed.withOpacity(0.2),
                                    numcolor: AppTheme.nearlyRed,
                                  );
                                }),
                          ),
                          // make sale
                          Container(
                            child: ListView.builder(
                                padding: EdgeInsets.all(0.0),
                                physics: BouncingScrollPhysics(),
                                itemCount: 6,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (_, index) {
                                  return ItemListSale(
                                    title: 'Incomes',
                                    date: '02-Oct-2020',
                                    color: AppTheme.greenBackground,
                                    numcolor: AppTheme.greenColor,
                                  );
                                }),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ProfitSeries {
  final int profit;
  final String month;

  ProfitSeries(this.profit, this.month);
}
