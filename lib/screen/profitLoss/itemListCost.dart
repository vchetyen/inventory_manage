import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class ItemListCost extends StatefulWidget {
  final String title;
  final String date;
  final Color color;
  final Color numcolor;
  const ItemListCost({
    Key key,
    this.title,
    this.date,
    this.color,
    this.numcolor,
  }) : super(key: key);

  @override
  _ItemListCostState createState() => _ItemListCostState();
}

class _ItemListCostState extends State<ItemListCost> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 375,
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 1, color: AppTheme.nearlyBlack.withOpacity(0.2)))),
      child: ListTile(
        onTap: () {},
        title: Text(
          widget.date,
          // style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,fontFamily: AppTheme.fontName),
          style: GoogleFonts.poppins(
            color: AppTheme.nearlyBlack.withOpacity(0.3),
            fontSize: 13.0,
          ),
        ),
        subtitle: Text(
          widget.title,
          style: GoogleFonts.poppins(
              color: AppTheme.nearlyBlack,
              fontSize: 16.0,
              fontWeight: FontWeight.bold),
        ),
        trailing: Container(
          width: 56.8,
          height: 23,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(35.0), color: widget.color),
          child: Center(
              child: Text(
                "-200\$",
                // style: TextStyle(
                //     color: widget.numcolor,
                //     fontWeight: FontWeight.bold,
                //     fontSize: 13.0),
                style: GoogleFonts.poppins(
                    color: widget.numcolor,
                    fontSize: 13.0,
                    fontWeight: FontWeight.bold),
              )),
        ),
      ),
    );
  }
}