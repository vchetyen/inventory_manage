import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

class ProfileAccount extends StatefulWidget {
  final String imageProfile;
  final String accountName;
  final String position;
  const ProfileAccount(
      {Key key, this.accountName, this.position, this.imageProfile})
      : super(key: key);
  @override
  _ProfileAccountState createState() => _ProfileAccountState();
}

class _ProfileAccountState extends State<ProfileAccount> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
        color: AppTheme.nearlyBlack,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
            right: 10,
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              width: size.width,
              height: size.height / 7,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Profile Picture
                  ClipRRect(
                    borderRadius: BorderRadius.circular(30.0),
                    child: Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage(widget.imageProfile))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10, left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.accountName,
                          style: GoogleFonts.poppins(
                              fontSize: 14,
                              color: AppTheme.mainColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.position,
                          style: GoogleFonts.poppins(
                              fontSize: 12, color: Colors.grey),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
