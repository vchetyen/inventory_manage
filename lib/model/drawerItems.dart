import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:inventory_manage/model/theme.dart';

Widget createDrawerItems(
    {IconData icon,
    String text,
    GestureTapCallback onTap,
    TextStyle titleStyle}) {
  return ListTile(
    title: Row(
      children: [
        Icon(
          icon,
          color: AppTheme.mainColor,
          size: 30,
        ),
        Padding(
          padding: EdgeInsets.only(left: 30.0),
          child: Text(text,
              style: GoogleFonts.poppins(
                  color: AppTheme.mainColor,
                  fontSize: 14,
                  fontWeight: FontWeight.bold)),
        ),
      ],
    ),
    onTap: onTap,
  );
}
