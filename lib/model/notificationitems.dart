import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Widget titleNotification(String titlenotification) {
  return Text(
    titlenotification,
    style: GoogleFonts.poppins(
        fontSize: 20, fontWeight: FontWeight.bold, letterSpacing: 1.0),
  );
}

Widget stockinstockNotification(String text, String qty, String times,
    Color qtycolor, BuildContext context) {
  Size size = MediaQuery.of(context).size;
  return ListTile(
    title: Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(40.0),
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/productPage/cb1.jpg"))),
            width: 70,
            height: 70,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
            width: size.width / 2,
            height: 70,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                text,
                style: GoogleFonts.poppins(fontSize: 15),
              ),
              Text(
                times,
                style: GoogleFonts.poppins(color: Colors.grey, fontSize: 15),
              )
            ])),
        SizedBox(
          width: 10,
        ),
        Container(
          width: 60,
          height: 60,
          child: Center(
            child: Text(qty,
                style: TextStyle(
                    color: qtycolor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18)),
          ),
        )
      ],
    ),
  );
}

Widget stockalertNotification(
    String texts, String times, String images, BuildContext context) {
  Size size = MediaQuery.of(context).size;
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Row(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(40.0),
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover, image: AssetImage(images))),
            width: 70,
            height: 70,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
              // width: size.width / 1.5,
              height: 70,
              // color: Colors.green[100],
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: size.width * 0.7,
                          child: Text(
                            texts,
                            style: GoogleFonts.poppins(),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      times,
                      style:
                          GoogleFonts.poppins(color: Colors.grey, fontSize: 15),
                    )
                  ])),
        ),
      ],
    ),
  );
}
