import 'package:flutter/material.dart';
import 'package:inventory_manage/screen/homepage/homepage.dart';
import 'package:inventory_manage/screen/slashScreen/splashScreen.dart';
import 'screen/productPage/productPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //home: MyHomePage(),
      //home: InventoryPage(),
      home: SplashScreen(),
      //home: ProductPage(),
    );
  }
}
